﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInView : MonoBehaviour
{
    [SerializeField]
    private Camera _cam;//Camera used to detect enemys on the screen 
    private bool _addOnlyOne;//one enemy once in the list

    void Start()
    {
        _cam = Camera.main;
        _addOnlyOne = true;
    }
    
    void Update()
    {
        Detection();
    }

    private void Detection()
    {
        //Vector3 with Dimension based on the Camera's viewport
        Vector3 _enemyPosition = _cam.WorldToViewportPoint(gameObject.transform.position);

        //If the  X and Y values are between 0 and 1, the enemy is on screen
        bool _onScreen = _enemyPosition.z > 0 && _enemyPosition.x > 0 && _enemyPosition.x < 1 && _enemyPosition.y > 0 && _enemyPosition.z < 50;
        
        //If the enemy is on the screen add it to the list
        if (_onScreen &&  _addOnlyOne)
        {
            _addOnlyOne = false;
            TargetMarkController.nearByEnemies.Add(this);
            TargetHealthBar.enemies.Add(this.gameObject);
        }
        if (!_onScreen)
        {
            TargetMarkController.nearByEnemies.Remove(this);
            TargetHealthBar.enemies.Remove(this.gameObject);
            _addOnlyOne = true;
        }
    }
}
