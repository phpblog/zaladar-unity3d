﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    Animator _anim;
    GameManager gm;
    public string EnemyName = "Enemy";

    public bool IsEnemyAlive;
    public Stats st;
    [SerializeField]
    private GameObject _target;
    public float EnemyMaxHealth = 2500; 
    public float EnemyHealth = 2500;
    public float Dmg = 20;
    public float AttackCooldown = 0f;
    public float enemyLevel = 1;
    void Start()
    {
        _anim = GetComponent<Animator>();
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        IsEnemyAlive = true;
        st = GameObject.FindGameObjectWithTag("Player").GetComponent<Stats>();
        _target = GameObject.FindGameObjectWithTag("Player");
        EnemyHealth = EnemyMaxHealth;
    }
    
    void Update()
    {
        AttackCooldown -= Time.deltaTime;
        if (_target != null && EnemyHealth > 0)
        {
            if (st.Health > 0)
            {
                Attack();
            }
        }
        if (EnemyHealth <= 0)
        {
            EnemyHealth = 0;
            IsEnemyAlive = false;
            gm._enemyNeed = gm._enemyNeed + 1;
            Destroy(this.gameObject);
        }
    }
    public void Attack()
    {
        Vector3 direction = _target.transform.position - this.transform.position;
        float angle = Vector3.Angle(direction, this.transform.forward);
        if (Vector3.Distance(_target.transform.position, this.transform.position) < 2 && angle < 90)
        {
            if (AttackCooldown <= 0f)
            {
                _anim.Play("Attack");
                st.Health -= Dmg;
                AttackCooldown = 1f;
            }   
        }
    }
}
