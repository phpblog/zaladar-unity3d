﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetMarkController : MonoBehaviour
{

    [SerializeField]
    private Camera _cam;//Main Camera
    public EnemyInView _target;// Current focused enemy in the list
    [SerializeField]
    private Image _image; //Crosshair

    public bool _lockedOn;//Keeps track of lock on Status

    public int _lockedEnemy;//Tracks wich Enemy in Lists is the current target

    public static List<EnemyInView> nearByEnemies = new List<EnemyInView>();//List of nearby enemies
    
    void Start()
    {
        _cam = Camera.main;
        _lockedOn = false;
        _image.enabled = false;
    }
    
    void Update()
    {
        if (nearByEnemies != null)
        {
            TabTarget();
        }
        else
        {
            return;
        }
    }
    private void TabTarget()
    {
        //press Tab key to list lock on
        if (Input.GetKeyDown(KeyCode.Tab) && !_lockedOn)
        {
            if (nearByEnemies.Count >= 1)
            {
                _lockedOn = true;
                _image.enabled = true;

                //Lock on the first enemy in List by default
                _lockedEnemy = 0;
                _target = nearByEnemies[_lockedEnemy];
            }
            else
            {
                return;
            }
        }
        else//Turn off lock on when Tab is pressed or no more enemies are in the list
        {
            if ((Input.GetKeyDown(KeyCode.Escape) && _lockedOn) || nearByEnemies.Count == 0)
            {
                _lockedOn = false;
                _image.enabled = false;
                _lockedEnemy = 0;
                _target = null;
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab))//switch target in focus
        {
            if (_lockedEnemy == nearByEnemies.Count - 1)
            {
                //If end of list has been reached, start over
                _lockedEnemy = 0;
                _target = nearByEnemies[_lockedEnemy];
            }
            else
            {
                //Move to next enemy in list
                _lockedEnemy++;
                _target = nearByEnemies[_lockedEnemy];
            }
        }
        if (_lockedOn)
        {
            _target = nearByEnemies[_lockedEnemy];

            //Determin crosshair on the target location
            _image.transform.position = _cam.WorldToScreenPoint(_target.transform.position + new Vector3(0, 2, 0));

            //Rotate crosshair
            //gameObject.transform.Rotate(new Vector3(0, 0, 1));
        }
    }
}
