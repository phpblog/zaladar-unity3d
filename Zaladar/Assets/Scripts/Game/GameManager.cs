﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _spawnPoint;
    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private GameObject _enemy;
    [SerializeField]
    private Transform[] _enemySpawnLocation;
    public int _tabIndex = 0;
    private EnemyAI ea;
    private int _livingEnemy;
    public int _livingEnemyMax;
    public int _enemyNeed;
    private int i = 0;

    void Start()
    {
        //Instantiate(_player);
        _player.transform.position = _spawnPoint.transform.position;
        _player.transform.rotation = _spawnPoint.transform.rotation;
        _tabIndex = 0;
        //Instantiate(_enemy);
        //EnemySpawn();
        _livingEnemyMax = _enemySpawnLocation.Length;
    }
    
    void Update()
    {
        _enemyNeed = _livingEnemyMax - _livingEnemy;
        if (_enemyNeed > 0)
        {
            EnemySpawn();
            _enemyNeed -= 1;
            if (_enemyNeed <= 0)
            {
                _enemyNeed = 0;
            }
        }
    }

    private void EnemySpawn()
    {
        if (i <= _livingEnemyMax)
        {
            Instantiate(_enemy, _enemySpawnLocation[i].transform.position, _enemySpawnLocation[i].rotation);
            _tabIndex++;
            _livingEnemy++;
            i++;
        }
        else
        {
            i = 0;
        }
    }
}
