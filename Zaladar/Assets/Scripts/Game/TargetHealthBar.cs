﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetHealthBar : MonoBehaviour
{
    TargetMarkController tmc;
    private EnemyStats est;
    public GameObject MyTarget;
    public Image Targeting;
    public Image TargetHealthbar;
    public Text TargetText;
    public Text TargetName;
    private float ratio;
    public static List<GameObject> enemies = new List<GameObject>();
    void Start()
    {
        TargetSetInactive();
        tmc = GetComponent<TargetMarkController>();
    }
    
    void Update()
    {
        if (tmc._lockedOn && tmc._target != null)
        {
            TargetInfo();
        }
        else
        {
            TargetSetInactive();
        }
    }
    public void TargetInfo()
    {
        MyTarget = enemies[tmc._lockedEnemy];
        if (MyTarget != null)
        {
            est = MyTarget.GetComponent<EnemyStats>();
            TargetSetActive();
        }
        else
        {
            return;
        }
    }

    public void TargetSetInactive()
    {
        Targeting.enabled = false;
        TargetHealthbar.enabled = false;
        TargetText.enabled = false;
        TargetName.enabled = false;


        Targeting.enabled = false;
        TargetHealthbar.enabled = false;
        TargetText.text = "";
        TargetName.text = "";
    }
    public void TargetSetActive()
    {
        Targeting.enabled = true;
        TargetHealthbar.enabled = true;
        TargetText.enabled = true;
        TargetName.enabled = true;

        TargetName.text = est.EnemyName;
        TargetText.text = (est.EnemyHealth + "/" + est.EnemyMaxHealth);
        ratio = est.EnemyHealth / est.EnemyMaxHealth;
        TargetHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
    }
}
