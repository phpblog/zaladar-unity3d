﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    
    public GameObject Target;

    private TargetHealthBar thb;
    private EnemyStats est;
    [SerializeField]
    private Transform _spawnLocation;
    [SerializeField]
    private float _currentLevel = 1;
    public float MaxLevel = 60;

    public float _expGain = 35;
    public float _maxExp = 100;

    public float Health = 1000;
    public float maxHealth = 1000;
    public float Stamina = 100;
    public float Dmg = 250;

    public bool IsPlayerAlive;
    void Start()
    {
        IsPlayerAlive = true;
        Health = maxHealth;
        thb = GetComponent<TargetHealthBar>();
    }
    
    void Update()
    {
        if (Health <= 0)
        {
            Health = 0;
            IsPlayerAlive = false;
        }
        //TabTarget();
    }
    public void TabTarget()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Target = GameObject.FindGameObjectWithTag("Enemy");
            est = Target.GetComponent<EnemyStats>();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Target = null;
            thb.TargetSetInactive();
        }
    }
}
