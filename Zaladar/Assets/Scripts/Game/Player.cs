﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Stats st;
    public Transform _spawn;
    #region Declaration
    //Character declaration
    #region Character
    [SerializeField]
    private CharacterController _controller;
    private Animator _anim;

    public float Speed = 5f;
    [SerializeField]
    private float _gravity = 14f;
    [SerializeField]
    private float _jumpforce = 3f;
    private float _jumpVelocity;
    private float _turnSpeed = 5f;
    #endregion

    //Camera declaration
    #region Camera
    [SerializeField]
    private Transform _playerCamera;
    [SerializeField]
    private Transform _centerPoint;

    [SerializeField]
    private float _distance;
    [SerializeField]
    private float _mindistance;
    [SerializeField]
    private float _maxdistance;
    
    private float _height;
    [SerializeField]
    private float _minHeight;
    [SerializeField]
    private float _maxHeight;

    [SerializeField]
    private float _orbitingSpeed;
    [SerializeField]
    private float _verticalSpeed;

    private bool _mouseLock = false;
    #endregion
    #endregion

    void Start()
    {
        st = GetComponent<Stats>();
        _anim = GetComponent<Animator>();
        _controller = GetComponent<CharacterController>();
    }
    
    void Update()
    {
        if (st.Health == 0)
        {
            this._controller.transform.position = _spawn.position;
            st.Health = st.maxHealth;
        }
        else
        {
            MoveAndMouse();
        }
        CameraCenter();
        //MoveAndMouse();
    }

    private void MoveAndMouse()
    {
        
        if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            //left click holding normal character movement, Camera movement
            if (Input.GetMouseButton(0))
            {
                MouseScrolling();
                MouseLookLeftClickHolding();
                MouseLockTurnOn();
                MovementNormal();
            }

            //right click camera & character rotation, 
            //A & D in movement now not rotate instead move the character left & right
            if (Input.GetMouseButton(1))
            {
                MouseScrolling();
                MouseLookRightClickHolding();
                MouseLockTurnOn();
                MovementRight();
            }
        }
        else
        {
            MouseLockTurnOff();
            //Movement, Camera behind the character
            MouseScrolling();
            _playerCamera.LookAt(_centerPoint);
            _playerCamera.position = _centerPoint.position + _centerPoint.forward * -1 * _distance + Vector3.up * _height;
            MovementNormal();
        }
    }

    #region Move

    private void MovementNormal()
    {
        // Move Jump
        float _horizontalInput = Input.GetAxis("Horizontal");
        float _verticalInput = Input.GetAxis("Vertical");
        if (_controller.isGrounded)
        {
            _jumpVelocity = -_gravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _jumpVelocity = _jumpforce;
            }
        }
        else
        {
            _jumpVelocity -= _gravity * Time.deltaTime;
        }
        //Move forward & backward
        Vector3 direction = Vector3.zero;
        direction = new Vector3(0, _jumpVelocity, _verticalInput);
        Vector3 velocity = (direction * Speed) * Time.deltaTime;
        velocity = transform.TransformDirection(velocity);
        _controller.Move(velocity);
        //Character rotation
        Vector3 _turn = new Vector3(0, _horizontalInput * _turnSpeed, 0);
        _controller.transform.eulerAngles += _turn;

        //Send value to animator
        _anim.SetFloat("BlendY", _horizontalInput);
        _anim.SetFloat("BlendY", _verticalInput);
    }

    private void MovementRight()
    {
        // Mouve Jump
        float _horizontalInput = Input.GetAxis("Horizontal");
        float _verticalInput = Input.GetAxis("Vertical");
        if (_controller.isGrounded)
        {
            _jumpVelocity = -_gravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _jumpVelocity = _jumpforce;
            }
        }
        else
        {
            _jumpVelocity -= _gravity * Time.deltaTime;
        }
        //Movement
        Vector3 direction = Vector3.zero;
        direction = new Vector3(_horizontalInput, _jumpVelocity, _verticalInput);
        Vector3 velocity = (direction * Speed) * Time.deltaTime;
        velocity = transform.TransformDirection(velocity);
        _controller.Move(velocity);

        //Send value to animator
        _anim.SetFloat("BlendY", _horizontalInput);
        _anim.SetFloat("BlendY", _verticalInput);
    }
    #endregion
    
    #region Camera
    
    private void MouseLookRightClickHolding()
    {
        //rotate the character on the Y
        float _mouseX = Input.GetAxis("Mouse X");
        Vector3 newRotation = transform.localEulerAngles;
        newRotation.y += _mouseX * 6;
        transform.localEulerAngles = newRotation;
        //rotate the camera sideways
        _centerPoint.position = gameObject.transform.position + new Vector3(0, 1.15f, 0);
        _height += Input.GetAxis("Mouse Y") * Time.deltaTime * _verticalSpeed * -1;
        _height = Mathf.Clamp(_height, _minHeight, _maxHeight);
        _playerCamera.position = _centerPoint.position + _centerPoint.forward * -1 * _distance + Vector3.up * _height;
        _playerCamera.LookAt(_centerPoint);

    }


    private void MouseLookLeftClickHolding()
    {
        // CenterPoint place
        _centerPoint.position = gameObject.transform.position + new Vector3(0, 1.15f, 0);
        //Mouse Horizontal look
        float _mouseY = Input.GetAxis("Mouse X") * Time.deltaTime * _orbitingSpeed;
        _centerPoint.eulerAngles += new Vector3(0, _mouseY, 0);

        //Mouse Vertical look
        _height += Input.GetAxis("Mouse Y") * Time.deltaTime * _verticalSpeed * -1;
        _height = Mathf.Clamp(_height, _minHeight, _maxHeight);
        _playerCamera.position = _centerPoint.position + _centerPoint.forward * -1 * _distance + Vector3.up * _height;
        _playerCamera.LookAt(_centerPoint);
    }

    private void MouseScrolling()
    {
        //Mouse Distance manipulate
        if (_mindistance < _distance)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                _distance--;
            }
        }
        if (_maxdistance > _distance)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                _distance++;
            }
        }
    }

    private void MouseLockTurnOn()
    {
        //cursor lock & invisible
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _mouseLock = true;
    }
    private void MouseLockTurnOff()
    {
        //cursor unlock & visible
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        _mouseLock = false;
    }

    private void CameraCenter()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            Vector3 _charTurn = this.transform.eulerAngles;
            _centerPoint.eulerAngles = _charTurn;
            _playerCamera.LookAt(_centerPoint);
            _playerCamera.position = new Vector3(0, 1.57f, -2f);
        }
    }

    #endregion
}
