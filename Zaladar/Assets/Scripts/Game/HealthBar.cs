﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Image currentHealthbar;
    public Text ratioText;

    private float ratio;
    private Stats st;
    private float hitpoint = 150;
    private float maxHitpoint = 150;

    void Start()
    {
        st = GetComponent<Stats>();
        ratioText.text = (st.Health + "/" + st.maxHealth);
        Ratio();
    }
    
    void Update()
    {
        ratioText.text = (st.Health + "/" + st.maxHealth);
        Ratio();
    }
    private void Ratio()
    {
        ratio = st.Health / st.maxHealth;
        currentHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
    }
}
