﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    EnemyStats est;
    private GameManager _gm;
    private Stats st;
    public GameObject _player;
    public bool IsDead = false;

    private Animator _anim;
    private NavMeshAgent _agent;
    private float _distance;

    public int _tabIndex;
    private float _gravity = 3f;

    
    void Start()
    {
        est = this.GetComponent<EnemyStats>();
        _gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        if (_gm == null)
        {
            Debug.Log("Nincs meg");
        }
        _player = GameObject.FindGameObjectWithTag("Player");
        _anim = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        st = _player.GetComponent<Stats>();
        _tabIndex = _gm._tabIndex;
    }
    
    void Update()
    {
        if (est.IsEnemyAlive)
        {
            if (st.IsPlayerAlive)
            {
                Movement();
            }
            else
            {
                _anim.SetBool("isIdle", true);
                _anim.SetBool("isWalking", false);
                //_anim.SetBool("isAttacking", false);
            }
        }
        else
        {
            _anim.SetBool("isIdle", true);
            _anim.SetBool("isWalking", false);
            //_anim.SetBool("isAttacking", false);
        }
    }

    private void Movement()
    {
        Vector3 direction = _player.transform.position - this.transform.position;
        float angle = Vector3.Angle(direction, this.transform.forward);
        if (Vector3.Distance(_player.transform.position, this.transform.position) < 5 && angle < 90)
        {
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);

            _anim.SetBool("isIdle", false);
            if (direction.magnitude > 0.7)
            {
                this.transform.Translate(0, 0, 0.1f);
                _anim.SetBool("isWalking", true);
                //_anim.SetBool("isAttacking", false);
            }
            else
            {
                //_anim.SetBool("isAttacking", true);
                _anim.SetBool("isWalking", false);
            }
        }
        else
        {
            _anim.SetBool("isIdle", true);
            //_anim.SetBool("isWalking", false);
            _anim.SetBool("isAttacking", false);
        }
    }
}
