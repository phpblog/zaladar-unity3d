﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : MonoBehaviour
{
    Animator _anim;
    TargetHealthBar thb;
    EnemyStats est;
    Stats st;
    private GameObject _target;
    public float AttackDistance = 20f;
    private bool _canAttack;

    void Start()
    {
        thb = GetComponent<TargetHealthBar>();
        //est = GetComponent<EnemyStats>();
        st = GetComponent<Stats>();
        _anim = GetComponent<Animator>();
        _canAttack = false;
    }

    void Update()
    {
        if (thb.MyTarget != null)
        {
            _target = thb.MyTarget;
            est = _target.GetComponent<EnemyStats>();
            KeyOne();
        }
        else
        {
            return;
        }
    }
    public void KeyOne()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (Vector3.Distance(this.transform.position, _target.transform.position) <= AttackDistance)
            {
                _anim.Play("Attack");
                _canAttack = true;
                est.EnemyHealth -= st.Dmg;
            }
            else
            {
                _canAttack = false;
                OutOfRange();
            }
        }
    }
    private void OutOfRange()
    {
        Debug.Log("Enemy is out of range.");
    }
    private void NoEnemySelected()
    {
        Debug.Log("No enemy selected");
    }
}
